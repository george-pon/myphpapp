FROM php:5.6.30-apache

ENV MY_PHP_APP_VERSION build_target
ENV MY_PHP_APP_VERSION latest
ENV MY_PHP_APP_VERSION stable
ENV MY_PHP_APP_VERSION v0.2.0
ENV MY_PHP_APP_IMAGE myphpapp

COPY php/index.php /var/www/html
COPY php/redirect.php /var/www/html
COPY php/main.php /var/www/html

COPY php/dir /var/www/html/dir
COPY php/3rddir /var/www/html/3rddir

EXPOSE 80
