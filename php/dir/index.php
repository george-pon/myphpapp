<?php
    session_start();
?>

<html>
<head><title>PHP APP v0.2 dir/index</title></head>
<body>

<?php

    print('PHP APP v0.2 dir/index <br/>'."\n");

    $time = date_default_timezone_set("Y/m/d H:i"); //アクセス時刻
    $ip = getenv("REMOTE_ADDR"); //IPアドレス
    $remotehost = getenv("REMOTE_HOST"); //ホスト名
    $referer = getenv("HTTP_REFERER"); //リファラ
    $request_host = $_SERVER['HTTP_HOST']; // request header の HOST
    if($referer == "") {
      $referer = "no_referer";
    }
    $log = $time .", REMOTE_ADDR:". $ip . ",  REMOTE_HOST:". $remotehost. ",  REFERER:". $referer . ",  Request Header Host:" . $request_host;
    print('access log:'.$log.'</br>'."\n");

    print('request headers:</br>'."\n");
    foreach (getallheaders() as $name => $value) {
        print("    $name: $value  </br>\n");
    }
    print('<br/>'."\n");
 
    if (!isset($_COOKIE["PHPSESSID"])){
        print('Cookie PHPSESSID is null. ');
    }else {
        print('Cookie PHPSESSID is '.$_COOKIE["PHPSESSID"].'. ');
    }
    print('<br/>'."\n");

    session_start();
    if (!isset($_SESSION['count'])) {
        print('First Time. Start Session. ');
        $_SESSION['count'] = 0;
    }else{
        $_SESSION['count'] = $_SESSION['count'] + 1;
        print('Session Continued.<br/>');
        print('session count is '.$_SESSION['count'].' . ');
    }

?>

</body>
</html>
