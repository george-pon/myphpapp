<?php
    session_start();

    $request_host = $_SERVER['HTTP_HOST']; // request header の HOST

    // リダイレクト先のURLへ転送する
    $url = 'http://' . $request_host . '/3rddir/main.php';
    header('Location: ' . $url, true, 301);

    // すべての出力を終了
    exit;
?>
