<?php
    session_start();

    $request_host = $_SERVER['HTTP_HOST']; // request header の HOST

    // リダイレクト先のURLへ転送する
    $url = 'http://' . $request_host . '/main.php';
    header('Location: ' . $url, true, 301);

    // すべての出力を終了
    exit;
?>
