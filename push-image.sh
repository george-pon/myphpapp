#!/bin/bash
#
# docker image build
#
set -e

cd "$(dirname "$0")"

TAG_LIST=${TAG_LIST:-$(awk '/ENV MY_PHP_APP_VERSION*/ {print $3}' Dockerfile)}
TAG_REMAIN=${TAG_REMAIN:-$(echo $TAG_LIST | awk '{ for ( i = 2 ; i <= NF ; i++ ) { print $i; } }' )}
TAG=${TAG:-$(echo $TAG_LIST | awk '{print $1;}')}
REPO=${REPO:-$(awk '/ENV MY_PHP_APP_IMAGE/ {print $3}' Dockerfile)}
# push する先のサーバを指定
# REPO_SERV=${REPO_SERV:-reg.local:5000/}
# minikube環境 の場合、sudo不要。
if [ x"$MINIKUBE_EXE_NAME"x != x""x ]; then
    DOCKER_SUDO_CMD=
else
    DOCKER_SUDO_CMD=sudo
fi
IMAGE=${REPO}:${TAG}

if [ ! -z "$HTTP_PROXY" ]; then
    BUILD_ARG_OPT="$BUILD_ARG_OPT  --build-arg HTTP_PROXY=$HTTP_PROXY"
fi
if [ ! -z "$HTTPS_PROXY" ]; then
    BUILD_ARG_OPT="$BUILD_ARG_OPT  --build-arg HTTPS_PROXY=$HTTPS_PROXY"
fi
if [ ! -z "$http_proxy" ]; then
    BUILD_ARG_OPT="$BUILD_ARG_OPT  --build-arg http_proxy=$http_proxy"
fi
if [ ! -z "$https_proxy" ]; then
    BUILD_ARG_OPT="$BUILD_ARG_OPT  --build-arg https_proxy=$https_proxy"
fi
if [ ! -z "$NO_PROXY" ]; then
    BUILD_ARG_OPT="$BUILD_ARG_OPT  --build-arg NO_PROXY=$NO_PROXY"
fi
if [ ! -z "$no_proxy" ]; then
    BUILD_ARG_OPT="$BUILD_ARG_OPT  --build-arg no_proxy=$no_proxy"
fi

if [ -z "${REPO_SERV}" ]; then
    echo Skip push "${REPO_SERV}${IMAGE}"
else
    for i in $TAG_LIST
    do
        $DOCKER_SUDO_CMD docker tag "${IMAGE}" "${REPO_SERV}${REPO}:${i}"
        echo Done tag "${REPO_SERV}${REPO}:${i}"
        $DOCKER_SUDO_CMD docker push "${REPO_SERV}${REPO}:${i}"
        echo Done push "${REPO_SERV}${REPO}:${i}"
    done
fi
