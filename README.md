# myphpapp

my php app

* display session info

### accessible URL

* http://localhost/index.php
* http://localhost/redirect.php  ( redirect 301 to /main.php )
* http://localhost/main.php
* http://localhost/dir/index.php
* http://localhost/dir/redirect.php  ( redirect 301 to /dir/main.php )
* http://localhost/dir/main.php
* http://localhost/dir/subdir/index.php
* http://localhost/dir/subdir/redirect.php  ( redirect 301 to /dir/subdir/main.php )
* http://localhost/dir/subdir/main.php
* http://localhost/dir/subdir/3rddir/index.php
* http://localhost/dir/subdir/3rddir/redirect.php  ( redirect 301 to /dir/subdir/3rddir/main.php )
* http://localhost/dir/subdir/3rddir/main.php
* http://localhost/3rddir/index.php
* http://localhost/3rddir/redirect.php  ( redirect 301 to /3rddir/main.php )
* http://localhost/3rddir/main.php
