#!/bin/bash
#
# docker image build
#
set -e

cd "$(dirname "$0")"

TAG_LIST=${TAG_LIST:-$(awk '/ENV MY_PHP_APP_VERSION*/ {print $3}' Dockerfile)}
TAG_REMAIN=${TAG_REMAIN:-$(echo $TAG_LIST | awk '{ for ( i = 2 ; i <= NF ; i++ ) { print $i; } }' )}
TAG=${TAG:-$(echo $TAG_LIST | awk '{print $1;}')}
REPO=${REPO:-$(awk '/ENV MY_PHP_APP_IMAGE/ {print $3}' Dockerfile)}

# dockerの実行にsudoコマンドが必要なら書く  DOCKER_SUDO_CMD=sudo
DOCKER_SUDO_CMD=

IMAGE=${REPO}:${TAG}

if [ ! -z "$HTTP_PROXY" ]; then
    BUILD_ARG_OPT="$BUILD_ARG_OPT  --build-arg HTTP_PROXY=$HTTP_PROXY"
fi
if [ ! -z "$HTTPS_PROXY" ]; then
    BUILD_ARG_OPT="$BUILD_ARG_OPT  --build-arg HTTPS_PROXY=$HTTPS_PROXY"
fi
if [ ! -z "$http_proxy" ]; then
    BUILD_ARG_OPT="$BUILD_ARG_OPT  --build-arg http_proxy=$http_proxy"
fi
if [ ! -z "$https_proxy" ]; then
    BUILD_ARG_OPT="$BUILD_ARG_OPT  --build-arg https_proxy=$https_proxy"
fi
if [ ! -z "$NO_PROXY" ]; then
    BUILD_ARG_OPT="$BUILD_ARG_OPT  --build-arg NO_PROXY=$NO_PROXY"
fi
if [ ! -z "$no_proxy" ]; then
    BUILD_ARG_OPT="$BUILD_ARG_OPT  --build-arg no_proxy=$no_proxy"
fi

if [ ! -z "$no_cache" ]; then
    BUILD_ARG_OPT="$BUILD_ARG_OPT  --no-cache=$no_cache"
fi

$DOCKER_SUDO_CMD docker build -t "${IMAGE}" . $BUILD_ARG_OPT
echo Done building "${IMAGE}"

for i in $TAG_REMAIN
do
    $DOCKER_SUDO_CMD docker tag "${IMAGE}" "${REPO}:${i}"
    echo Done tag "${REPO}:${i}"
done

